package fr.uavignon.ceri.tp2.data;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

public interface BookDao {
    @Insert
    void insertBook(Book book);


    @Query("Select * from books where id =:id")
    List<Book> getBook(long id);

    @Query("delete from books where id=:id")
    void deleteBook(long id);

    @Update
    void updateBook(Book book);


    @Query("select * from books")
    LiveData<List<Book>> getAllBooks();


    @Delete
    public void deleteAllBooks();
}

