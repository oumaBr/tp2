package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
import java.util.concurrent.Executor;

public class BookRepository {
    private MutableLiveData<List<Book>> findResults= new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;
    private BookDao bookDao;
    public BookRepository(Application application){
        BookRoomDatabase db= BookRoomDatabase.getDatabase(application);
        bookDao=db.bookDao();
        allBooks=bookDao.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks(){
        return allBooks;
    }
    public MutableLiveData<List<Book>> getSearchResults() {
        return findResults;
    }


    /*
    public void insertBook(Book newbook) {
        Executor databaseWriteExecutor = null;
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newbook);
        });
    }
    public void deleteBook(long id){}


     */
}

